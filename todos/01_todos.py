todos = []
todoNum = 1
line = '='*6

while True:
    print(line + ' 일정관리 시스템 ' + line)
    print('1. 일정 등록')
    print('2. 일정 수정')
    print('3. 일정 삭제')
    print('4. 일정 조회')
    print('5. 일정 전체삭제')
    print('6. 종료')

    menu = input('메뉴를 선택하세요. : ')
    
    if menu == '1':
        todo_dict = {}
        title = input('일정을 입력하세요. : ')
        todo_dict['todoNum'] = todoNum
        todo_dict['title'] = title
        todos.append(todo_dict)

        print('일정이 등록됐습니다.')
        todoNum += 1

    elif menu == '2':
        if not todos:
            print('수정할 일정이 없습니다.')
            continue

        _todoNum = input('수정할 일정번호를 입력하세요. : ')
        has_num = False
        k = 0

        while not _todoNum.isdigit():
            print('숫자를 입력하세요.')
            _todoNum = input('번호를 입력하세요. : ')
        
        for todo in todos:
            if todo['todoNum'] == int(_todoNum):
                has_num = True
                break
            k += 1
        
        if not has_num:
            print('존재하지 않는 일정번호입니다.')
        
        else:
            title = input('수정할 내용을 입력하세요. : ')
            todos[k]['title'] = title
            print('일정 {0}이 수정됐습니다.'.format(_todoNum))       
           
    elif menu == '3':
        if not todos:
            print('삭제할 일정이 없습니다.')
            continue

        _todoNum = input('삭제할 일정번호를 입력하세요. : ')
        has_num = False
        k = 0

        while not _todoNum.isdigit():
            print('숫자를 입력하세요.')
            _todoNum = input('번호를 입력하세요. : ')
        
        for todo in todos:
            if todo['todoNum'] == int(_todoNum):
                has_num = True
                break
            k += 1
        
        if not has_num:
            print('존재하지 않는 일정번호입니다.')

        else:
            _todoNum = todos[k]['todoNum']
            del todos[k]
            print('일정 {0}이 삭제됐습니다.'.format(_todoNum))
            
            """
            if todos: # 일정 재정렬
                end = len(todos)

                for i in range(0, end):
                    todos[i]['todoNum'] = i + 1
                
                todoNum = end + 1
            
            """

    elif menu == '4':
        if not todos: print('조회할 일정이 없습니다.')
        else:
            for todo in todos:
                print('일정{0}.  {1}'.format(todo['todoNum'], todo['title']))

    elif menu == '5':
        if not todos: print('삭제할 일정이 없습니다.')
        else: 
            todos.clear()
            todoNum = 1
            print('전체삭제가 완료됐습니다.')

    elif menu == '6':
        print('일정관리 시스템을 종료합니다.')
        break

    else: print('1 ~ 6사이의 값을 입력하세요.')